
# lua-TestLongString

---

## Overview

lua-TestLongString is a port of the Perl5 module
[Test::LongString](https://metacpan.org/pod/Test::LongString).

It is an extension of
[lua-TestMore](https://fperrad.frama.io/lua-TestMore).

It provides functions for comparing and testing strings
that are not in plain text or are especially long.

## Status

lua-TestLongString is in beta stage.

It's developed for Lua 5.1, 5.2, 5.3 & 5.4.

## Download

lua-TestLongString source can be downloaded from
[Framagit](https://framagit.org/fperrad/lua-TestLongString).

## Installation

The easiest way to install lua-TestLongString is to use LuaRocks:

```sh
luarocks install lua-testlongstring
```

or manually, with:

```sh
make install
```

## Copyright and License

Copyright &copy; 2009-2023 Fran&ccedil;ois Perrad

This library is licensed under the terms of the MIT/X11 license,
like Lua itself.
