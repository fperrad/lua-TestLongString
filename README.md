
lua-TestLongString
==================

Introduction
------------

lua-TestLongString is a port of the Perl5 module [Test::LongString](https://metacpan.org/pod/Test::LongString).

It is an extension of [lua-TestMore](https://fperrad.frama.io/lua-TestMore/).

It provides functions for comparing and testing strings
that are not in plain text or are especially long.

Links
-----

The homepage is at <https://fperrad.frama.io/lua-TestLongString/>,
and the sources are hosted at [https://framagit.org/fperrad/lua-TestLongString/>).

Copyright and License
---------------------

Copyright (c) 2009-2023 Francois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.

